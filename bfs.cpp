// Undirected graph
// This code works for nodes starting from 0

#include<iostream>
#include<vector>
#include<queue>
using namespace std;

void bfs(int src, vector<int> graph[], vector<bool> &visited) {
    visited[src] = true;
    queue<int> q;
    q.push(src);
    while(!q.empty()) {
        int curr = q.front();
        cout << curr << " ";
        q.pop();
        for(auto s: graph[curr]) {
            if(!visited[s]) {
                visited[s] = true;
                q.push(s);
            }
        }
    }
}

int main() {
    int nodes, edges;
    cin >> nodes >> edges;
    vector<int> graph[nodes+1];
    vector<bool> visited(nodes+1, false);
    for(int i=0;i<edges;i++) {
        int from, to;
        cin >> from >> to;
        graph[from].push_back(to);
        graph[to].push_back(from);
    }
    bfs(0, graph, visited);
    return 0;
}