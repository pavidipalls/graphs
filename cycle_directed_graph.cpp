//Detect cycle in a directed graph
// This code works for only nodes starting from 0 or 1.

#include<iostream>
#include<vector>
using namespace std;

int cyclicHelper(int src, vector<int> graph[], vector<bool> &visited, vector<bool> &recStack) {
    if(!visited[src]) {
        visited[src] = true;
        recStack[src] = true;
        for(auto s:graph[src]) {
            if(!visited[s] and cyclicHelper(s, graph, visited, recStack)) return true;
            else if(recStack[s]) return true;
        }
    }
    recStack[src] = false;
    return false;
}

int isCyclic(int s, vector<int> graph[]) {
    vector<bool> visited(s+1, false);
    vector<bool> recStack(s+1, false);
    for(int i=0;i<s;i++) {
        if(cyclicHelper(i, graph, visited ,recStack)) {
            return true;
        }
    }
    return false;
}

int main() {
    int n, e;
    cin >> n >> e;
    vector<int> graph[n+1];
    for(int i=0;i<e;i++) {
        int u, v; cin >> u >> v;
        graph[u].push_back(v);
    }
    if(isCyclic(n, graph)) cout << "Is Detected";
    else cout << "NOT Detected";
    return 0;
}