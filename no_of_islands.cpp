/*
https://leetcode.com/problems/number-of-islands/
Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.

An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

 

Example 1:

Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1
Example 2:

Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3
*/
#include<iostream>
#include<vector>
using namespace std;

void dfs(int x, int y, vector<vector<char>> &mat) {
    if(x < 0 || x >= mat.size() || y < 0 || y >= mat[0].size() || mat[x][y] == '0')
        return;

    mat[x][y] = '0';
    dfs(x-1, y, mat);   // Moving LEFT
    dfs(x+1, y, mat);   // Moving RIGHT
    dfs(x, y-1, mat);   // Moving UP
    dfs(x, y+1, mat);   // Moving DOWS
}

int main() {
    int rows, cols, islands = 0;
    cout << "Enter rows and cols : " << endl;
    cin >> rows >> cols;
    cout << "Enter the matrix : " << endl;
    vector<vector<char>> mat(rows, vector<char> (cols, '0'));
    for(int i=0;i<rows;i++) {
        for(int j=0;j<cols;j++) {
            cin >> mat[i][j];
        }
    }
    for(int i=0;i<rows;i++) {
        for(int j=0;j<cols;j++) {
            if(mat[i][j] == '1') {
                ++islands;
                dfs(i, j, mat);
            }
        }
    }
    cout << "Total Number of islands : " << islands << endl;
    return 0;
}

/*
INPUT & OUTPUT :
1.
Enter rows and cols :
4 5
Enter the matrix :
1 1 1 1 0
1 1 0 1 0
1 1 0 0 0
0 0 0 0 0
Total Number of islands : 1

2.
Enter rows and cols :
4 5
Enter the matrix :
1 1 0 0 0
1 1 0 0 0
0 0 1 0 0
0 0 0 1 1
Total Number of islands : 3
*/