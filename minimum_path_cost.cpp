/*
https://www.geeksforgeeks.org/minimum-cost-of-simple-path-between-two-nodes-in-a-directed-and-weighted-graph/

Input : V = 5, E = 6
        s = 0, t = 2
    graph[][] =      0   1   2   3   4  
                 0  INF -1  INF  1  INF
                 1  INF INF -2  INF INF
                 2  -3  INF INF INF INF
                 3  INF INF -1  INF INF
                 4  INF INF INF  2  INF
 
Output : -3 
Explanation : 
The minimum cost simple path between 0 and 2 is given by:
0 -----> 1 ------> 2 whose cost is (-1) + (-2) = (-3). 

Input : V = 5, E = 6
        s = 0, t = 4
    graph[][] =      0   1   2   3   4  
                 0  INF -7  INF -2  INF
                 1  INF INF -11 INF INF
                 2  INF INF INF INF INF
                 3  INF INF INF  3  -4
                 4  INF INF INF INF INF
 
Output : -6
Explanation : 
The minimum cost simple path between 0 and 2 is given by:
0 -----> 3 ------> 4 whose cost is (-2) + (-4) = (-6). 
*/

//C++ code for printing Minimum Cost
// Simple Path between two given nodes
// in a directed and weighted graph
// Above example is source and the given code works for only positive weights 
// and INF is replaced with 0
// This is JUPAY coding challenge 2nd round question and answer 
#include<iostream>
#include<vector>
#include<climits>
using namespace std;
int graph[5001][5001]={0};
bool visited[5001];

int dijsktra(int source, int destination, int v, vector<int> member) {
	int flag = 0;
    if(source == destination) return 0;
    visited[source] = true;
    int min_time = INT_MAX;
    for(auto i:member) {
        if(graph[source][i] != 0 and !visited[i]) {
		    flag = 1;
            int path_curr = dijsktra(i, destination, v, member);
            if(path_curr < INT_MAX) {
                min_time = min(min_time, graph[source][i]+path_curr);
            }
        }
    }
    visited[source] = false;
	if(!flag) return  -1;
    return min_time;
}

int main() {
    int v;
    cin >> v;
    vector<int> member;
    for(int i=0;i<v;i++) {
        int m; cin >> m;
        member.push_back(m);
    }
    int e; cin >> e;
    for(int i=0;i<e;i++) {
        int source, destination, time_taken;
        cin >> source >> destination >> time_taken;
        graph[source][destination] = time_taken;
    }
    int follower, following;
    cin >> follower >> following;
    cout << "Min_Cost_Path : " << endl;
    cout << dijsktra(follower, following, v, member);
    return 0;
}

/*
INPUT & OUTPUT :
4
2
5
7
9
4
2 9 2
7 2 3
7 9 7
9 5 1
7
9
Min_Cost_Path :
5
*/