/*
https://leetcode.com/problems/flood-fill/
https://www.youtube.com/watch?v=RwozX--B_Xs

733. Flood Fill (LEETCODE)
An image is represented by an m x n integer grid image where image[i][j] represents the pixel value of the image.

You are also given three integers sr, sc, and newColor. You should perform a flood fill on the image starting from the pixel image[sr][sc].

To perform a flood fill, consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color), and so on. Replace the color of all of the aforementioned pixels with newColor.

Return the modified image after performing the flood fill.

 

Example 1:


Input: image = [[1,1,1],[1,1,0],[1,0,1]], sr = 1, sc = 1, newColor = 2
Output: [[2,2,2],[2,2,0],[2,0,1]]
Explanation: From the center of the image with position (sr, sc) = (1, 1) (i.e., the red pixel), all pixels connected by a path of the same color as the starting pixel (i.e., the blue pixels) are colored with the new color.
Note the bottom corner is not colored 2, because it is not 4-directionally connected to the starting pixel.
Example 2:

Input: image = [[0,0,0],[0,0,0]], sr    = 0, sc = 0, newColor = 2
Output: [[2,2,2],[2,2,2]]
 

Constraints:

m == image.length
n == image[i].length
1 <= m, n <= 50
0 <= image[i][j], newColor < 216
0 <= sr(source row) < m
0 <= sc(source column) < n
*/

#include<iostream>
#include<vector>
using namespace std;

void dfs(vector<vector<int>> &image, int sr, int sc, int source, int rows, int cols, int newColor) {
    if(sr < 0 || sc < 0 || sr >= rows || sc >= cols) return;
    else if(image[sr][sc] != source) return;
    
    image[sr][sc] = newColor;
    dfs(image, sr-1, sc, source, rows, cols, newColor);   // Moving TOP
    dfs(image, sr+1, sc, source, rows, cols, newColor);   // Moving BOTTOM
    dfs(image, sr, sc-1, source, rows, cols, newColor);   // Moving LEFT
    dfs(image, sr, sc+1, source, rows, cols, newColor);   // Moving RIGHT
}

int main() {
    int rows, cols;
    cin >> rows >> cols;
    vector<vector<int>> image(rows, vector<int>(cols, 0));
    for(int i=0;i<rows;i++) {
        for(int j=0;j<cols;j++)
            cin >> image[i][j];
    }
    int sr, sc, newColor; cin >> sr >> sc >> newColor;
    cout << "Before flood_fill : " << endl;
    for(int i=0;i<rows;i++) {
        for(int j=0;j<cols;j++)
            cout << image[i][j] << " ";
        cout << endl;
    }
    int source = image[sr][sc];
    dfs(image, sr, sc, source, rows, cols, newColor);
    cout << "After flood_fill : " << endl;
    for(int i=0;i<rows;i++) {
        for(int j=0;j<cols;j++)
            cout << image[i][j] << " ";
        cout << endl;
    }
    return 0;
}

/*
OUTPUT - 1 :

3 3
1 1 1
1 1 0
1 0 1
1 1 2
Before flood_fill :
1 1 1
1 1 0
1 0 1
After flood_fill :
2 2 2
2 2 0
2 0 1

OUTPUT - 2 :
2 3
0 0 0
0 0 0
0 0 2
Before flood_fill :
0 0 0
0 0 0
After flood_fill :
2 2 2
2 2 2
*/