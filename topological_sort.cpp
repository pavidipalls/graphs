/*Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering of vertices 
such that for every directed edge u v, vertex u comes before v in the ordering. 
Topological Sorting for a graph is not possible if the graph is not a DAG.*/
// https://www.youtube.com/watch?v=qe_pQCh09yU

#include<iostream>
#include<stack>
#include<vector>
using namespace std;

stack<int> s;

void topological_sort_dfs(int src, vector<int> graph[], vector<bool> &visited) {
    visited[src] = true;
    for(auto s: graph[src]) {
        if(!visited[s]) {
            topological_sort_dfs(s, graph, visited);
        }
    }
    s.push(src);
}

int main() {
    int nodes, edges;
    cin >> nodes >> edges;
    vector<int> graph[nodes+1];
    vector<bool> visited(nodes+1, false);
    for(int i=0;i<edges;i++) {
        int u, v; cin >> u >> v;
        graph[u].push_back(v);
    }
    for(int i=0;i<nodes;i++)
        if(!visited[i])
            topological_sort_dfs(i, graph, visited);
    cout << "Toplogical Sort : ";
    while(!s.empty()) {
        cout << s.top() << " ";
        s.pop();
    }
    return 0;
}