// Undirected graph
// This code only works for the nodes starting from 0 (zero)
#include<iostream>
#include<vector>
using namespace std;

void dfs(int src, vector<int> graph[], vector<bool> &visited) {
    visited[src] = true;
    cout << src << " ";
    for(auto s:graph[src]) {
        if(!visited[s]) {
            dfs(s, graph, visited);
        }
    }
}

int main() {
    int nodes, edges;
    cin >> nodes >> edges;
    vector<int> graph[nodes+1];
    vector<bool> visited(nodes+1, false);
    for(int i=0;i<edges;i++) {
        int from, to;
        cin >> from >> to;
        graph[from].push_back(to);
        graph[to].push_back(from);
    }
    dfs(0, graph, visited);
    return 0;
}