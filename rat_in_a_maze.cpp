/*Consider a rat placed at (0, 0) in a square matrix of order N * N. It has to reach the destination at (N - 1, N - 1). Find all possible paths that the rat can take to reach from source to destination. The directions in which the rat can move are 'U'(up), 'D'(down), 'L' (left), 'R' (right). Value 0 at a cell in the matrix represents that it is blocked and rat cannot move to it while value 1 at a cell in the matrix represents that rat can be travel through it.
Note: In a path, no cell can be visited more than one time.

Example 1:

Input:
N = 4
m[][] = {{1, 0, 0, 0},
         {1, 1, 0, 1}, 
         {1, 1, 0, 0},
         {0, 1, 1, 1}}
Output:
2
Explanation:
The rat can reach the destination at 
(3, 3) from (0, 0) by two paths - DRDDRR 
and DDRDRR, when printed in sorted order 
we get DDRDRR DRDDRR.
Example 2:
Input:
N = 2
m[][] = {{1, 0},
         {1, 0}}
Output:
0
Explanation:
No path exists and destination cell is 
blocked.
Your Task:  
You don't need to read input or print anything. Complete the function printPath() which takes N and 2D array m[ ][ ] as input parameters and returns the list of paths in lexicographically increasing order. 
Note: In case of no path, return an empty list. The driver will output "-1" automatically.*/

#include<bits/stdc++.h>
using namespace std;
void dfs(vector<vector<int>>mat,int n,vector<vector<int>>&visited,int i,int j,int &c){
    if(n == 1 and mat[i][j] == 0) return;
    if(i==n-1 && j==n-1){
        c++;
        return;
    }
    visited[i][j] = true;
    if(i+1<n && !visited[i+1][j]&& mat[i+1][j]==1){
        dfs(mat,n,visited,i+1,j,c);
        
    }
    if(i-1>=0 && !visited[i-1][j]&& mat[i-1][j]==1){
        dfs(mat,n,visited,i-1,j,c);
        
    }
    if(j+1<n && !visited[i][j+1]&& mat[i][j+1]==1){
        dfs(mat,n,visited,i,j+1,c);
        
    }
    if(j-1>=0 && !visited[i][j-1]&& mat[i][j-1]==1){
        dfs(mat,n,visited,i,j-1,c);
        
    }
    visited[i][j] = false;
}
int main() {
    int n;
    cin>>n;
    vector<vector<int>>mat(n,vector<int>(n));
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            int t;
            cin>>t;
            mat[i][j]=t;
        }
    }
    vector<vector<int>>visited(n,vector<int>(n,0));
    int c=0;
    if(mat[0][0])
     dfs(mat,n,visited,0,0,c);
    
	cout<<c<<endl;
	return 0;
}